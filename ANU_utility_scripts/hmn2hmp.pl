#!/usr/bin/env perl
my $header=0;
my %iupac=qw/CT R TC R AG Y GA Y AT W TA W GC S CG S TG M GT M CA K AC K/;
while(<>) {
	chomp;
	s/#//g;
	s/:/_/g;
	my @line=split/\t/;
#	if($line[0]=~/^TP\d+/) { #only for UNEAK
	next if $line[0] eq 'rs' && $header++;
	
	if($line[1]=~/\// && $line[2]=~/^\d+$/ && $line[3]=~/^\d+$/) {
		my($a,$b,%c)=split/\//,$line[1];
		$c{$line[$_]}++ for 11..$#line;
		$line[1]=join'/',($a,$b)=($b,$a) if $c{$b}>$c{$a};
		$line[$_]=$line[$_] eq '.'?'N':$line[$_] eq '0'?$a:$line[$_] eq '2'?$b:$iupac{$a.$b} for 11..$#line;
	}
	print join("\t",@line),"\n";	
}
