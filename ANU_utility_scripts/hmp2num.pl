#!/usr/bin/env perl
my $header=0;
while(<>) {
	chomp;
	s/#//g;
	s/:/_/g;
	my @line=split/\t/;
#	if($line[0]=~/^TP\d+/) { #only for UNEAK
	if($line[0] eq 'rs') {
		print "$_\n" unless $header++;
	}elsif($line[1]=~/\// && $line[2]=~/^\d+$/ && $line[3]=~/^\d+$/) {
		my %c=();
		my($a,$b)=split/\//,$line[1];
		$c{$line[$_]}++ for 11..$#line;
		$line[1]=join'/',($a,$b)=($b,$a) if $c{$b}>$c{$a};
		$line[$_]=$line[$_] eq 'N'?'.':$line[$_] eq $a?'0':$line[$_] eq $b?'2':'1' for 11..$#line;
		print join("\t",@line),"\n";
	}
}
