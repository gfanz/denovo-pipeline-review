#!/usr/bin/env perl
for my $file(@ARGV) {
	my $outFile=$file;
	die "$file is not a hapmap!" unless -s $file && $outFile=~s/\.hmp.txt$/\.hmn\.txt/;
	`/home/data/projects/borevitz/hmp2num.pl $file > $outFile`;
	print `wc -l $outFile`;
}
