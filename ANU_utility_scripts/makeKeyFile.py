#!/usr/bin/env python
import csv
import itertools
import argparse
NOT_FOUND='?'
DELIM=' / '

def main():
    """
        Build a key file that contains the mapping of
        samples to barcodes for TASSEL.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--enzyme', default='TGCAG', 
            help='The enzyme site for cutting, defaults to TGCAG for PST1')
    parser.add_argument('-b', '--barcode', 
            help='Path to barcode plate layouts file, usually 1152Barcode.txt')
    parser.add_argument('-s', '--smp', required=True, 
            help='Path to sample plate layout file')
    parser.add_argument('-p', '--plates', required=True, 
            help='A string of space separated plate numbers '+\
            'to be demultiplexed, e.g "5 6 9 10"')
    parser.add_argument('-l', '--lanes', required=True,
            help='A string of space separated lane numbers. '+\
            'There should be one for each plate listed in --plates '+\
            'e.g. "1 1 1 1". These lanes numbers must match the lane '+\
            'number field in the sequence files given to TASSEL')
    parser.add_argument('-P', '--Project', required=True,
            help='Project name. Must match the first field of the '+\
            'sequence files given to TASSEL')
    parser.add_argument('-k', '--keyfile', required=True,
            help='Path to output key file')
    args = parser.parse_args()

    #enzyme_site='CGG' #CCGG
    #enzyme_site='TGCAG' #PstI
    enzyme_site = args.enzyme

    plate_list = map(int, args.plates.strip().split(' '))
    lane_list = map(int, args.lanes.strip().split(' '))

    project_name = args.Project
    key_fn = args.keyfile
    
    # Read barcodes
    reader=csv.reader(open(args.barcode, 'rb'), delimiter='\t')

    barcode={}
    plate=''
    for row in reader:
        if row[0]>'':
            if row[1]=='1' and row[12]=='12':
                plate=row[0]
            else:
                for i in range(12):
                    lbar,rbar=row[i+1].upper().split(DELIM,2)
                    if rbar>'':
                        rbar='TG'+rbar #ugly hack to ensure uniqueness to TASSEL
                    barcode["%s%s%02d"%(plate,row[0],i+1)]=lbar+rbar

    sample={}
    plate=''
    # read in sample plate file
    reader=csv.reader(open(args.smp,'rb'), delimiter='\t') #edit here
    for row in reader:
    #    print row
        if row[0]>'':
            if row[1]=='1' and row[12]=='12':
                plate=row[0]
            else:
                for i in range(12):
                    sample["%s%s%02d"%(plate,row[0],i+1)]=row[i+1]
    with open(key_fn, 'w') as f:
        f.write("Flowcell\tLane\tBarcode\tSample\tPlateName\tRow\tColumn\n")
        for lane, plate in zip(lane_list, plate_list):
            for j in xrange(ord('A'),ord('H')+1):
                for i in range(12):
                    cell="PI%d%c%02d"%(plate,j,i+1)
                    if sample[cell] > '':
                        outline = '\t'.join(map(str, [project_name, lane, 
                                barcode[cell], sample[cell], plate, j, i+1]))
                        f.write(outline + '\n')

if __name__ == '__main__':
    main()
