#!/usr/bin/env python3

import sys
import argparse

"""
Barcodes:
PI1     1       2       3       4       5       6       7       8       9       10      11      12
A       CTCG / ATGAAAG  TGCA / ATGAAAG  ACTA / ATGAAAG  CAGA / ATGAAAG  AACT / ATGAAAG  GCGT / ATGAAAG  CGAT / ATGAAAG  GTAA / ATGAAAG  AGGG / ATGAAAG  GATG / ATGAAAG  TCAG / ATGAAAG  TGCG / ATGAAAG
B       CGCTT / CTCG    TCACG / CTCG    CTAGG / CTCG    ACAAA / CTCG    TTCTG / CTCG    AGCCG / CTCG    GTATT / CTCG    CTGTA / CTCG
    ACCGT / CTCG    GCTTA / CTCG    GGTGT / CTCG    AGGAT / CTCG
C       ATTGA / ACGACTAG        CATCT / ACGACTAG        CCTAG / ACGACTAG        GAGGA / ACGACTAG        GGAAG / ACGACTAG        GTCAA / ACGACTAG        TAATA / ACGACTAG        TACAT / ACGACTAG        TCGTT / ACGACTAG        GGTTGT / ACGACTAG       CCAGCT / ACGACTAG
       TTCAGA / ACGACTAG
D       TAGGAA / ACTA   GCTCTA / ACTA   CCACAA / ACTA   CTTCCA / ACTA   GAGATA / ACTA   ATGCCT / ACTA   AGTGGA / ACTA   ACCTAA / ACTA
   ATATGT / ACTA   ATCGTA / ACTA   CATCGT / ACTA   CGCGGT / ACTA
E       CTATTA / TGCGA  GCCAGT / TGCGA  GGAAGA / TGCGA  GTACTT / TGCGA  GTTGAA / TGCGA  TAACGA / TGCGA  TGGCTA / TGCGA  TATTTTT / TGCGA CTTGCTT / TGCGA ATGAAAG / TGCGA AAAAGTT / TGCGA GAATTCA / TGCGA
F       GAACTTG / CGCTT GGACCTA / CGCTT GTCGATT / CGCTT AACGCCT / CGCTT AATATGG / CGCTT ACGTGTT / CGCTT ATTAATT / CGCTT ATTGGAT / CGCTT CATAAGT / CGCTT CGCTGAT / CGCTT CGGTAGA / CGCTT CTACGGA / CGCTT
G       GCGGAAT / GGTTGT        TAGCGGA / GGTTGT        TCGAAGA / GGTTGT        TCTGTGA / GGTTGT        TGCTGGA / GGTTGT        ACGACTAG / GGTTGT       TAGCATGG / GGTTGT       TAGGCCAT / GGTTGT       TGCAAGGA / GGTTGT       TGGTACGT / GGTTGT       TCTCAGTG / GGTTGT
       CCGGATAT / GGTTGT
H       CGCCTTAT / CCAGCT       AACCGAGA / CCAGCT       ACAGGGAA / CCAGCT       ACGTGGTA / CCAGCT       CCATGGGT / CCAGCT       CGCGGAGA / CCAGCT       CGTGTGGT / CCAGCT       GCTGTGGA / CCAGCT       GGATTGGT / CCAGCT       GTGAGGGT / CCAGCT       TATCGGGA / CCAGCT
       TTCCTGGA / CCAGCT
"""

def read_barcodes(barcode_fn):
    plate_row_col = {}  # (plate,row,col) = (fwdbar,revbar)
    with open(barcode_fn, 'r') as b:
        current_plate = None
        for line in b:
            l = line.strip()
            if l == '':
                continue
            cols = l.split('\t')
            if 'PI' in cols[0]:
                current_plate = cols[0].lstrip('PI')
                continue
            else:
                row = cols[0]
            for i, c in enumerate(cols):
                if i == 0:
                    continue
                fwdbar, revbar = c.split(' / ')
                plate_row_col[(current_plate,row,i)] = (fwdbar + '\t' + revbar)
    return plate_row_col

"""
Plate 1 1       2       3       4       5       6       7       8       9       10      11      12      PstI Adapter Plate 9
A       Otway-ER1179    Powel-ER1116    Powel-ER1113    Powel-ER1098    Tool-ER1035     Powel-ER1105    Otway-ER1152    Otway-ER1182
    Otway-ER1174    Tool-ER1038     Powel-ER1108    Powel-ER1102x   
B       Maroo-ER1080    Powel-ER1091    Otway-ER1162    Tool-ER0994     Tool-ER1004     Tool-ER0995     Camb-ER1066     Otway-ER1175
    Tool-ER1013     Tool-ER1030     Camb-ER1068     Otway-ER1143    
C       Powel-ER1124    Powel-ER1132    Tool-ER0987     Tool-ER0983     Powel-ER1127    Camb-ER1070     Camb-ER1065     Tool-ER1032
     Tool-ER0996     Otway-ER1158    Tool-ER0990     Tool-ER1003     
D       Otway-ER1164    Otway-ER1181    Maroo-ER1079    Otway-ER1161    Powel-ER1117    Otway-ER1139    Tool-ER1026     Tool-ER1017
     OShan-ER1082    Tool-ER0999     Tool-ER1006     Otway-ER1148    
E       Powel-ER1128    Otway-ER1147    Tool-ER1007     Otway-ER1146    Powel-ER1110    Otway-ER1168    Tool-ER1010     Maroo-ER1078
    Otway-ER1149    Tool-ER0997     Otway-ER1166    Tool-ER1000x    
F       Powel-ER1123    Otway-ER1163    Otway-ER1173y   Tool-ER1008     Powel-ER1086    Powel-ER1109    Tool-ER0992     Powel-ER1095
    Tool-ER1000y    Otway-ER1177    OShan-ER1075    Otway-ER1142    
G       Powel-ER1125    Powel-ER1096    Otway-ER1156    Powel-ER1120    Powel-ER1136    Tool-ER0991     Tool-ER1028     Tool-ER1022
     Tool-ER1037     Powel-ER1090    Powel-ER1129    Otway-ER1150    
H       Powel-ER1115    Powel-ER1088    Powel-ER1126    Powel-ER1114    Tool-ER0989     Tool-ER1016     Tool-ER1009     Tool-ER1021x
    Tool-ER1034     Powel-ER1118    Powel-ER1122    -BLANK 
"""

def read_samples(samples_fn, barcodes, prefix):
    sample_barcodes = {}  # [sample] = (fwdbar,revbar)
    with open(samples_fn, 'r') as s:
        current_plate = None
        current_adapters = None
        for line in s:
            l = line.strip()
            if l == '':
                continue
            cols = l.split('\t')
            if 'Plate' in cols[0]:
                if current_adapters is not None:
                    with open(prefix+'_' + current_plate + '.axe', 'w') as out:
                        out.write('Barcode1\tBarcode2\tID\n')
                        for s in sample_barcodes:
                            out.write(sample_barcodes[s] + '\t' + s + '\n')
                    sample_barcodes = {} 
                current_adapters = cols[13].lstrip('PI')
                current_plate = cols[0].lstrip('Plate ')
                continue
            else:
                row = cols[0]
            for i, c in enumerate(cols):
                if i == 0:
                    continue
                if c == '' or 'blank' in c.lower() or 'empty' in c.lower() or 'none' in c.lower():
                    continue
                sample = c
                sample_barcodes[sample] = barcodes[(current_adapters,row,i)]

    with open(prefix+'_' + current_plate + '.axe', 'w') as out:
        out.write('Barcode1\tBarcode2\tID\n')
        for s in sample_barcodes:
            out.write(sample_barcodes[s] + '\t' + s + '\n')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sample_layout', required=True, help='Path to plated sample layout file')
    parser.add_argument('-b', '--barcodes', required=True, help='Path to barcodes file')
    parser.add_argument('-o', '--output_prefix', required=True, help='Path to prefix for output axe file')
    args = parser.parse_args()

    barcodes_plate_row_col = read_barcodes(args.barcodes)
    #print (barcodes_plate_row_col)

    # read samples and output an axe file for each plate
    read_samples(args.sample_layout, barcodes_plate_row_col, args.output_prefix)

if __name__ == '__main__':
   main()
