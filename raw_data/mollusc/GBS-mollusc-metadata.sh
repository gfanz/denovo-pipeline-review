#!/bin/bash
# This script pulls the meta data for the mollusc gbs data set and stores it in a text file.

wget "https://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=PRJNA285294&result=read_run&fields=instrument_model,library_name,library_selection,fastq_md5,fastq_ftp&download=txt" -O mollusc_metadata.txt 
